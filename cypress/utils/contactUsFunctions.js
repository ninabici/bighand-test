export const fillContactField = (element, text) => {
    cy.get("iframe").first().then($iframe => {
        const $frameContents = $iframe.contents();
        cy.wrap($frameContents.find(element)).type(text, { force: true });
    });
};

export const clickSubmitButton = () => {
    cy.get("iframe").first().then($iframe => {
        const $frameContents = $iframe.contents();
        cy.wrap($frameContents.find('.submit > input')).click({ force: true });
    });
};

export const readFrameMessages = (element, message) => {
    cy.get("iframe").first().then($iframe => {
        const $frameContents = $iframe.contents();
        cy.wrap($frameContents.find(element)).should('eq', message);
    });
};