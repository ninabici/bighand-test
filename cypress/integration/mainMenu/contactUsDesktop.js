import { home } from '../../utils/params'
import {
    contactUsButtonElement, firstName, firstNameInputFieldElement,
    mainErrorMessageElement, mainErrorMessage, lastNameErrorMessageEmenet, errorRequiredFieldMessage,
    companyErrorMessageElement, phoneErrorMessagEelement, emailErrorElement
} from '../../utils/conatctUsParams'
import { openContactUsPageDesktop } from '../../utils/menuFunctions';
import { fillContactField, clickSubmitButton, readFrameMessages } from '../../utils/contactUsFunctions'

describe('Check the contact us functionality on desktop', () => {
    beforeEach(function () {
        cy.visit(home);
        cy.wait(900);
        cy.viewport('macbook-15');
    });

    it('when only First Name is submitted', () => {
        openContactUsPageDesktop(contactUsButtonElement);
        cy.title().should('eq', 'Contact BigHand');
        cy.url().should('eq', `${home}/contact-us/`)
        fillContactField(firstNameInputFieldElement, firstName);
        cy.wait(900);
        clickSubmitButton();
        readFrameMessages(mainErrorMessageElement, mainErrorMessage);
        readFrameMessages(lastNameErrorMessageEmenet, errorRequiredFieldMessage);
        readFrameMessages(companyErrorMessageElement, errorRequiredFieldMessage);
        readFrameMessages(emailErrorElement, errorRequiredFieldMessage);
        readFrameMessages(phoneErrorMessagEelement, errorRequiredFieldMessage);
    });
});